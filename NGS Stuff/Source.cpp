#include "HeavensGate.hpp"

Utility::HookManager * pHookManager = 0;

FILE * pCon = 0;

bool HeavensGateCallback( int syscall_index, HeavensGate::CONTEXT64 * pCtx )
{
	auto GetProcessName = [ ]( DWORD pid )
	{
		std::wstring out( L"" );

		auto hndl = OpenProcess( PROCESS_QUERY_INFORMATION, 0, pid );
		if ( hndl && hndl != INVALID_HANDLE_VALUE )
		{
			wchar_t buf[ MAX_PATH ] = { 0 };
			UNICODE_STRING * buf2 = ( UNICODE_STRING * )buf;
			ULONG ret = 0;
			auto stat = NtQueryInformationProcess( hndl, ProcessImageFileName, buf2, sizeof( buf ), &ret );
			CloseHandle( hndl );
			if ( !stat )
			{
				out = std::wstring( buf2->Buffer );
				auto last = out.find_last_of( L'\\' );
				out = out.substr( last + 1 );
				//LogB( "Returning %S", out.c_str( ) );
				return out;
			}
			return out;
		}
		return out;
	};
	bool skipped = false;

	auto skip_call = [ pCtx, &skipped ]( DWORD ret_value )
	{
		pCtx->Rip += 3; // we havent called yet, so just ditch it loooooool
		pCtx->Rax = ret_value;
		skipped = true;
	};

	if ( syscall_index == 0x26 )
	{
		auto pid = ( DWORD )( ( uint64_t * )pCtx->R9 )[ 0 ];
		auto name = GetProcessName( pid );
		if ( !name.empty( ) )
		{
			LogB( "[ZwOpenProcess] PID: %d (%S)", pid, name.c_str( ) );

			if ( wcsstr( name.c_str( ), L"cheat" ) )
			{
				LogB( "Hiding cheat engine!" );
				skip_call( STATUS_ACCESS_DENIED );
			}
		}
	
	}
	else if ( syscall_index == 0x23 )
	{
		auto hHandle = ( HANDLE )pCtx->Rcx;
		auto pid = GetProcessId( hHandle );
		auto mod_name = Utility::GetModuleForAddress( pCtx->Rdx, pid );
		LogB( "[ZwQueryVirtualMemory] PID: %d (%S) | Class: %d | Loc: %llx [%S]", pid, GetProcessName( pid ).c_str( ), ( int )pCtx->R8, pCtx->Rdx, mod_name );
	}
	else if ( syscall_index == 0x3F )
	{
		auto handle = ( HANDLE )pCtx->Rcx;
		auto loc = pCtx->Rdx;
		auto pid = GetProcessId( handle );
		auto name = GetProcessName( pid );
		auto mod_name = Utility::GetModuleForAddress( loc, pid );
		LogB( "[ZwReadVirtualMemory] PID %d (%S) at location %llx which is module %S", pid, name.c_str( ), loc, mod_name );
	}
	else if ( syscall_index == 0x19 )
	{
		auto handle = ( HANDLE )pCtx->Rcx;
		auto info = ( long )pCtx->Rdx;
		auto pid = GetProcessId( handle );
		auto name = GetProcessName( pid );
		LogB( "[ZwQueryInformationProcess] PID: %d (%S) | Class: %d", pid, name.c_str( ), ( long )pCtx->Rdx );
	}
	else
	{
		LogB( "Dump struct for syscall %X", syscall_index );
		HeavensGate::DumpSyscallStruct( pCtx );
	}

	return skipped;
}

BOOL WINAPI DllMain( HMODULE hMod, DWORD dw, LPVOID )
{
	if ( dw == DLL_PROCESS_ATTACH )
	{
		AllocConsole( );
		freopen_s( &pCon, "CONOUT$", "w", stdout );

		//DisableThreadLibraryCalls( hMod );
		// hook some shenanigans here.
		pHookManager = new Utility::HookManager( );
		auto base = ( uintptr_t )GetModuleHandle( 0 );
		auto size = Utility::GetModuleSize( base );
		
		//NtSuspendProcess( ( HANDLE )-1 );
		HeavensGate::SetHeavensBridgeCallback( HeavensGateCallback );
		HeavensGate::HookGates( base ); // assuming we're in BlackCihper.aes
	}
	else if ( dw == DLL_THREAD_ATTACH )
	{
		LogB( "I see thread %d", GetCurrentThreadId( ) ); // i dont catch them here, theyre already spawned. just slightly too fast for me.

		// need to get its start addr, set a 0xCC, then set debug brekapoint afterwards.
		HeavensGate::AddThread( );
	}

	return TRUE;
}