#pragma once
#include <ntdll.h>
#include "../hook_lib/hook_lib/hooklib.hpp"
#include <string>

namespace Utility
{
	void SetByte( uintptr_t loc, unsigned char byte );
	uintptr_t GetModuleSize( uintptr_t base );
	const wchar_t * GetModuleForAddress( uintptr_t addr, int pid = 0 );
}
