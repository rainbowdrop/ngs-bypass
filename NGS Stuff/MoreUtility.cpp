#include "MoreUtility.hpp"

namespace Utility
{
	void SetByte( uintptr_t loc, unsigned char byte )
	{
		DWORD old = 0;
		VirtualProtect( ( void * )loc, 1, PAGE_EXECUTE_READWRITE, &old );
		*( unsigned char * )loc = byte;
		VirtualProtect( ( void * )loc, 1, old, &old );

		//auto success = FlushInstructionCache( ( HANDLE )-1, ( void * )loc, 0 );
	}

	uintptr_t GetModuleSize( uintptr_t base )
	{
		MEMORY_BASIC_INFORMATION region = { 0 };
		ULONG out_size = 0;
		auto stat = NtQueryVirtualMemory( ( HANDLE )-1, ( void * )base, MemoryRegionInformation, &region, sizeof( region ), &out_size );
		if ( stat != 0 )
		{
			if ( stat == 0xC0000004 )
			{
				Log( "Actual size is %d not %d", out_size, sizeof( region ) );
			}

			Log( "NTQVM ERROR: %X", stat );
			return 0;
		}

		return region.RegionSize;
	}
	const wchar_t * GetModuleForAddress( uintptr_t addr, int pid )
	{
		char buffer[ MAX_PATH * 2 ] = { 0 };
		PUNICODE_STRING us = 0;
		std::wstring big_name;
		SIZE_T ret_len = 0;
		HANDLE hndl = (HANDLE)-1;
		if ( pid )
		{
			hndl = OpenProcess( PROCESS_QUERY_INFORMATION, 0, pid );
			if ( !hndl || hndl == ( HANDLE )-1 )
			{
				LogB( "Failed to open handle to pid %d", pid );
				return nullptr;
			}
		}

		auto stat = NtQueryVirtualMemory( ( HANDLE )-1, ( void * )addr, MemoryMappedFilenameInformation, &buffer, sizeof( buffer ), &ret_len );
		CloseHandle( hndl );
		if ( stat != STATUS_SUCCESS )
		{
			//LogB( "Failed to query module name for addr %p with fail code %X", addr, stat );
		}
		else
		{
			//Log( "BUFFER AT %p", buffer );
			us = ( PUNICODE_STRING )( buffer );
			big_name = us->Buffer;
			auto fname_pos = big_name.find_last_of( L"\\" );
			big_name = big_name.substr( fname_pos + 1 );

			//LogB( "Return module for %p is %S", addr, big_name.c_str( ) );
			//MessageBoxA( 0, "CHECK ME", "HERRO", MB_ICONERROR );
		}

		return us ? big_name.c_str( ) : L"PRIVATE";
	}
}