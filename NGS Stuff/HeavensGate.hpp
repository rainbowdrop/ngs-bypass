#pragma once
#include "../hook_lib/hook_lib/hooklib.hpp"
#include "MoreUtility.hpp"
//#include <vector>
//#include <tuple>

//extern std::vector< std::tuple<uintptr_t, uintptr_t, unsigned char *> > modified_locations; // start, size, bytes

namespace HeavensGate
{
	// First step. detect heavens gates.
	struct CONTEXT64
	{
		uint64_t P1Home;
		uint64_t P2Home;
		uint64_t P3Home;
		uint64_t P4Home;
		uint64_t P5Home;
		uint64_t P6Home;
		uint32_t ContextFlags;
		uint32_t MxCsr;
		uint16_t SegCs;
		uint16_t SegDs;
		uint16_t SegEs;
		uint16_t SegFs;
		uint16_t SegGs;
		uint16_t SegSs;
		uint32_t EFlags;
		uint64_t Dr0;
		uint64_t Dr1;
		uint64_t Dr2;
		uint64_t Dr3;
		uint64_t Dr6;
		uint64_t Dr7;
		uint64_t Rax;
		uint64_t Rcx;
		uint64_t Rdx;
		uint64_t Rbx;
		uint64_t Rsp;
		uint64_t Rbp;
		uint64_t Rsi;
		uint64_t Rdi;
		uint64_t R8;
		uint64_t R9;
		uint64_t R10;
		uint64_t R11;
		uint64_t R12;
		uint64_t R13;
		uint64_t R14;
		uint64_t R15;
		uint64_t Rip;
	};

	void DumpSyscallStruct( HeavensGate::CONTEXT64 * pStruct );
	bool HookGates( uintptr_t module_base );
	using hb_callback = bool( * )( int syscall_index, HeavensGate::CONTEXT64 * pCtx );
	// same thing as just setting the ptr.
	void SetHeavensBridgeCallback( hb_callback pCallback );
	uintptr_t MatchSyscallToSymbol( int32_t syscall_index );
	int32_t GetSyscallForEip( uintptr_t eip );
	// Needs to be called from thread context
	void AddThread( );
}